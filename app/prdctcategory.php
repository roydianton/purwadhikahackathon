<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrdctCategory extends Model
{
    protected $table = "prdctcategory";
}
