<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\prdctcategory;

class frm_PrdctCatServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('product.frm_search', function($view) {
            $view->with('cat_list', prdctcategory::pluck('category', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
