<!DOCTYPE html>
<HTML>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<title>Palugada</title>

		{{-- Memanggil bootstrap --}}
		<link href="{{ asset('bootstrap-3.3.6/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">

		<!-- [if lt IE 9]>
			<script src="{{ asset('http://laravelapps.dev/js/html5shiv_3_7_2.min.js') }}"></script>
			<script src="{{ asset('http://laravelapp.dev/js/respond_1_4_2.min.js') }}"></script>
		<![endif] -->	</head>
	<body>
		<div class="container">
			@yield('main')
			@include('product.frm_search')
		</div>

		@yield('footer')
		
		<script src="{{ asset('js/jquery_2.2.1.min.js') }}"></script>
		<script src="{{ asset('bootstrap-3.3.6/js/bootstrap.min.js') }}"></script>
	</body>
</html>
