@extends('template')

@section('main')
	<div id="homepage">
		<h2>Palugada</h2>
		
		@if (!empty($cat_list))
			<table class="table">
				<thead>
					<tr></tr>
				</thead>
				<tbody>
					@foreach($cat_list as $category)
						<tr>
							<td>{{ $category->category }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<p>Tidak ada data Category</p>
		@endif
	</div>
@stop

@section('footer')
	@include('footer')
@stop
