<div id="searching">
	{!! Form::open(['url' => 'product/cari', 'method' => 'GET', 'id' => 'frm_search']) !!}
		<div class="row">
			<div class="col-md-2">
				<div class="input-category">
					{!! Form::select('cat_id', $cat_list, (!empty($cat_id) ? $cat_id : null), ['id' => 'cat_id', 'class' => 'form-control', 'placeholder => Category']) !!}
				</div>
			</div>

			<div class="col-md-8">
				<div class="input-group">
					{!! Form:: text('txtKey', (!empty($txtKey)) ? $txtKey : null, ['class' => 'form-control', 'placeholder' => 'Masukkan Produk Yang Dicari']) !!}
					<span class="input-group-btn">
						{!! Form::button('Cari', ['class' => 'btn btn-default', 'type' => 'submit']) !!}
						{!! Form::button('Daftar', ['class' => 'btn btn-default', 'type' => 'submit']) !!}
						{!! Form::button('Masuk', ['class' => 'btn btn-default', 'type' => 'submit']) !!}
					</span>
				</div>	
			</div>	
		</div>
	{!! Form::close() !!}
</div>