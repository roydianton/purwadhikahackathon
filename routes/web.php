<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@homepage');

Route::get('sampledata', function() {
	DB::table('prdctcategory')->insert([
		[
			'category' => 'Mainan & Hobi'
		],
		[
			'category' => 'Makanan & Minuman'
		],	
		[
			'category' => 'Buku'
		],			
		[
			'category' => 'Software'
		],
	]);
});